Ubuntu Core Vagrant
===================

This Vagrantfile is based on the [KVM installation](http://www.ubuntu.com/cloud/tools/snappy#snappy-local) of Ubuntu Core:  
No modifications, only converted to VirtualBox Vagrant Box.  
Port 80 is forwarded to 127.0.0.1:8090  
It is currently only optimized and tested on VirtualBox.

## Use this Vagrant Box

You need [Virtualbox](https://www.virtualbox.org) and [Vagrant](https://www.vagrantup.com) installed.

Create a new directory of your choice and boot Ubuntu Core with:

```bash
vagrant init therealmarv/ubuntu-core
vagrant up
```

Now login to your created Ubuntu Core VM:
```bash
vagrant ssh
```

Enjoy your VM and try out `snappy`! Username and password is both `ubuntu` like in the original [KVM image](http://www.ubuntu.com/cloud/tools/snappy#snappy-local).

## Development

Clone this repo and run `vagrant up` for testing.

Details are in the `build/HOWTO.md` file.

## Notes

Why Bitbucket? Because Github has a file limitiation of 100MB and the Box is more than 101MB big ;)

What is still needed is support for VirtualBox Guest Addition.
I hope it can be included for better development and shared folders.

Thanks,  
[@therealmarv](https://twitter.com/therealmarv)