How to build your own ubuntu-core.box
=====================================

This Vagrant Box was created from the Alpha 01 KVM image:  
http://cdimage.ubuntu.com/ubuntu-core/preview/ubuntu-core-alpha-01.img

All this instructions are only for Ubuntu.

QEMU Utils are needed:
```
sudo apt-get install qemu-utils
```

Download the image and convert it to vdi
```bash
wget http://cdimage.ubuntu.com/ubuntu-core/preview/ubuntu-core-alpha-01.img
qemu-img convert -O vdi ubuntu-core-alpha-01.img ubuntu-core-alpha-01.vdi
```

Create a new VM in the GUI of Virtualbox and use this vdi as harddisk.
Remember the name of this VM! You do not need to boot it up.
Turn off in the config of this VM all USB and audio.

Now export this Virtualbox VM to a Vagrant Box and include additional Vagrant
settings from `pkgvagrant`:
```bash
vagrant package --base <name-of-your-vbox> --vagrantfile pkgvagrant --output ubuntu-core.box
```

Done! :)

What is still needed is support for VirtualBox Guest Addition.
I hope it can be included for better development and shared folders.